# LTE-M Library for Docode-Modem

「どこでもでむ」で使用できるLTE-Mモジュール用ドライバです。

**主な注意点**

使用モジュール:　[Sierra Wireless HL7800-M](https://www.sierrawireless.com/products-and-solutions/embedded-solutions/products/hl7800/)

コールドスタート時のハード初期化待ち時間：　１０秒

アクティベーション：　５秒から２０秒ぐらい。成功しなかったらリトライが必要。

通信速度：　LTE-M自体は300kbpsらしいが、UARTの115200bpsとコマンドのやり取りがリミットとなる。

暗号化通信：　可能。暗号鍵を用意する必要があります。

どこでもでむMiniを使用する場合はファイルの修正とオプションの追加が必要です。下記を参照してください。

https://gitlab.com/docodemodem/docodemodem-libraries/docodemo_samd21

