/*
  HL7800Modem.cpp - Base class for HL7800(Sierra Wireless)
  Copyright (c) 2020 CircuitDesign,Inc.  All right reserved.

  Based programs are as follow
  https://github.com/SeeedJP/Wio_cell_lib_for_Arduino

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#include "HL7800Modem.h"
using namespace std;
#include <algorithm>

vector<string> split(const string &s, char delim)
{
    vector<string> elems;
    string item;
    for (char ch : s)
    {
        if (ch == delim)
        {
            if (!item.empty())
                elems.push_back(item);
            item.clear();
        }
        else
        {
            item += ch;
        }
    }
    if (!item.empty())
        elems.push_back(item);
    return elems;
}

char *serchResp(const char *str, const char *pattrnStart, const char *pattrnEnd)
{
    char *head = strstr(str, pattrnStart);
    if (head == NULL)
    {
        return NULL;
    }
    char *end = strstr(head + strlen(pattrnStart), pattrnEnd);
    if (end == NULL)
    {
        return NULL;
    }
    *end = 0;
    return head + strlen(pattrnStart);
}

HL7800Modem::HL7800Modem()
{
    _cmd = new char[256];
    _Activated = false;
    _EchoOn = true;

    _SecureMode = false;

    _checkSize = true;
    _recvSize = 0;
}

HL7800Modem::~HL7800Modem()
{
    delete[] _cmd;
}

// Private
bool HL7800Modem::readBinary(byte *data, int dataSize, unsigned long timeout)
{
    Stopwatch sw;
    sw.Restart();

    while (true)
    {
        int len = _pRead(data, dataSize, timeout / portTICK_RATE_MS);

        if (len > 0)
        {
            return true;
        }
        else
        {
            if (sw.ElapsedMilliseconds() >= timeout)
            {
                return false;
            }
        }
    }
}

bool HL7800Modem::readResponseUntilOk(unsigned long timeout)
{
    std::string response;

    Stopwatch sw;
    sw.Restart();

    while (true)
    {
        uint8_t rddata;
        int len = _pRead(&rddata, 1, 1);
        if (len > 0)
        {
            response.push_back((char)rddata);
        }
        else
        {
            if (sw.ElapsedMilliseconds() >= timeout)
            {
                return false;
            }
        }

        if (response.size() >= 2 && response.at(response.size() - 2) == CHAR_CR && response.at(response.size() - 1) == CHAR_LF)
        {
            DEBUG_PRINT(response.c_str());

            if (response.find("OK") != std::string::npos)
            {
                return true;
            }
            else if (response.find("ERROR") != std::string::npos)
            {
                return false;
            }

            response.clear();
        }
    }
}

bool HL7800Modem::readResponse(const char *pattern, unsigned long timeout, std::string *capture, bool WaitOK, bool checkNotIf)
{
    std::string response;
    Stopwatch sw;
    sw.Restart();

    if (capture != NULL)
        capture->clear();

    while (true)
    {
        if (response.size() >= (size_t)(RESPONSE_MAX_LENGTH + 2))
        {
            return false;
        }

        uint8_t rddata;
        int len = _pRead(&rddata, 1, 1);
        if (len > 0)
        {
            response.push_back((char)rddata);
        }
        else
        {
            if (sw.ElapsedMilliseconds() >= timeout)
            {
                return false;
            }
        }

        if (response.size() >= 2 && response.at(response.size() - 2) == CHAR_CR && response.at(response.size() - 1) == CHAR_LF)
        {
            DEBUG_PRINT(response.c_str());

            if (WaitOK && response.find("OK") != std::string::npos)
            {
                return true;
            }
            else if (response.find("ERROR") != std::string::npos)
            {
                return false;
            }
            else if (checkNotIf && response.find("KTCP_NOTIF") != std::string::npos)
            {
                return false;
            }

            if (capture != NULL)
            {
                if (response.find(pattern) != std::string::npos)
                {
                    response.resize(response.size() - 2);

                    if (capture->size() != 0)
                        *capture += "_"; // add a splitter char for AT+KTCPCFG?

                    *capture += response;

                    if (!WaitOK)
                        return true;
                }
            }
            else
            {
                if (response.find(pattern) != std::string::npos)
                {
                    return true;
                }
            }

            response.clear();
        }
    }
}

bool HL7800Modem::writeCommand(const char *command)
{
    int len = strlen(command);
    _pWrite((uint8_t *)command, len);

    return true;
}

bool HL7800Modem::writeBinary(const uint8_t *pData, int length, unsigned long timeout_ms)
{
    _pWrite((uint8_t *)pData, length);
    return true;
}

bool HL7800Modem::writeCommandAndWaitOK(const char *command, unsigned long timeout)
{
    if (!writeCommand(command))
        return false;
    return readResponseUntilOk(timeout);
}

bool HL7800Modem::writeCommandAndReadResponse(const char *command, const char *pattern, unsigned long timeout, std::string *capture)
{
    if (!writeCommand(command))
        return false;
    return readResponse(pattern, timeout, capture);
}

bool HL7800Modem::writeCommandAndWaitPattern(const char *command, const char *pattern, unsigned long timeout)
{
    if (!writeCommand(command))
        return false;
    return readResponse(pattern, timeout, NULL, false, false);
}

int HL7800Modem::socketOpenTCP(const char *host, int port)
{
    int sessionID = 0;

    std::string response;

    // Check connected host and port
    if (!writeCommandAndReadResponse("AT+KTCPCFG?\r", "+KTCPCFG:", MODEM_SHORT_TIMEOUT, &response))
        return 0;

    if (response.find("+KTCPCFG:") != std::string::npos)
    {
        int cfgCount = std::count(response.cbegin(), response.cend(), '_') + 1;
        vector<string> vCfg = split(response, '_');

        int i = 0;
        for (; i < cfgCount; i++)
        {
            vector<string> v = split(vCfg[i], ',');

            if (v[4].find(host) != std::string::npos && atoi(v[5].c_str()) == port)
            {
                char head = v[0].c_str()[v[0].size() - 1];
                sessionID = head - 0x30;
                if (v[1] == "1")
                {
                    // connected,but always not connected, then close anyway
                    socketCloseTCP(sessionID);
                    sessionID = 0;
                }
                break;
            }
        }

        if (i == 6)
        {
            // Reached Maximum session ID,but not connected. close all session;
            for (int j = 1; j <= 6; j++)
                socketCloseTCP(j);
            return 0;
        }
    }

    if (sessionID == 0)
    {
        // make socket
        if (_SecureMode)
        {
            sprintf(_cmd, "AT+KTCPCFG=1,3,\"%s\",%d\r", host, port);
        }
        else
        {
            sprintf(_cmd, "AT+KTCPCFG=1,0,\"%s\",%d\r", host, port);
        }

        if (!writeCommandAndReadResponse(_cmd, "+KTCPCFG:", MODEM_SHORT_TIMEOUT, &response))
            return 0;

        if (response.find("+KTCPCFG:") != string::npos)
        {
            char head = response.c_str()[response.size() - 1];
            sessionID = head - 0x30; // atoi(&head);
            DEBUG_PRINT("### sessionID: ");
            DEBUG_PRINTLN(sessionID);
        }
    }

    // magic delay...why? It makes stable connections.
    // maybe ssl connection need some delay.
    if (_SecureMode)
        delay(1000);

    // Start connection
    sprintf(_cmd, "AT+KTCPCNX=%d\r", sessionID);
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("connection error 1");
        sprintf(_cmd, "AT+KTCPDEL=%d\r", sessionID);
        writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT);
        return 0;
    }

    // wait connection ready
    sprintf(_cmd, "+KTCP_IND: %d,1", sessionID);
    if (!readResponse(_cmd, MODEM_CONNECT_TIMEOUT, NULL, false, true))
    {
        ERR_DEBUG_PRINTLN("connection error 2");
        sprintf(_cmd, "AT+KTCPDEL=%d\r", sessionID);
        writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT);
        return 0;
    }

    return sessionID;
}

int HL7800Modem::socketOpenUDP(const char *host, int port)
{
    return 0;
}

bool HL7800Modem::socketSendTCP(int sessionID, const byte *data, int dataSize)
{
    sprintf(_cmd, "AT+KTCPSND=%d,%d\r", sessionID, dataSize);
    if (!writeCommandAndReadResponse(_cmd, "CONNECT", MODEM_CONNECT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("socketSendTCP error 1");
        return false;
    }

    writeBinary(data, dataSize, MODEM_LONG_TIMEOUT);

    if (!writeCommandAndWaitOK(ATDATA_EOF, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("socketSendTCP error 2");
        return false;
    }

    return true;
}

bool HL7800Modem::socketSendUDP(int sessionID, const byte *data, int dataSize)
{
    return false;
}

int HL7800Modem::socketReceiveTCP(int sessionID, byte *data, int dataSize)
{
    std::string response;
    int rcvsize = 0;

    if (_checkSize)
    {
        sprintf(_cmd, "+KTCP_DATA: %d,", sessionID);
        if (!readResponse(_cmd, 1, &response, false))
        {
            // ERR_DEBUG_PRINTLN("socketReceiveTCP data 1");
            return 0;
        }

        vector<string> v = split(response, ',');
        rcvsize = atoi(v[1].c_str());
    }
    else
    {
        rcvsize = _recvSize;
        _checkSize = true;
    }

    if (rcvsize >= 1)
    {
        if (rcvsize > dataSize)
            return -1;

        sprintf(_cmd, "AT+KTCPRCV=%d,%d\r", sessionID, rcvsize);
        if (!writeCommandAndReadResponse(_cmd, "CONNECT", MODEM_LONG_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("socketReceiveTCP error 2");
            return -1;
        }
        if (!readBinary(data, rcvsize, MODEM_CONNECT_TIMEOUT))
            return -1;

        if (!readResponse("OK", MODEM_SHORT_TIMEOUT, NULL))
            return -1;
    }

    return rcvsize;
}
int HL7800Modem::socketReceiveUDP(int sessionID, byte *data, int dataSize)
{
    return 0;
}

bool HL7800Modem::socketCloseTCP(int sessionID)
{
    sprintf(_cmd, "AT+KTCPCLOSE=%d,1\r", sessionID);
    writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT);

    sprintf(_cmd, "AT+KTCPDEL=%d\r", sessionID);
    writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT);

    return true;
}
bool HL7800Modem::socketCloseUDP(int sessionID)
{
    return false;
}

// Public
bool HL7800Modem::init(UartWrite pWriteFunc, UarttRead pReadFunc, Channel band)
{
    // m_pUart = &pUart;
    if (pReadFunc == nullptr || pWriteFunc == nullptr)
        return false;

    _pWrite = pWriteFunc;
    _pRead = pReadFunc;

    if (!waitLteModem())
    {
        return false;
    }

#if 1
    // Set Hardware flow control
    if (!writeCommandAndWaitOK("AT&K3\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 0");
        return false;
    }
#endif

    // Disable Tx/Rx
    if (!writeCommandAndWaitOK("AT+CFUN=4\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 1");
        return false;
    }

    // Disable Network Registration Status
    if (!writeCommandAndWaitOK("AT+CEREG=0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 1");
        return false;
    }

    // No sleep
    if (!writeCommandAndWaitOK("AT+KSLEEP=2\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 1a");
        return false;
    }

    // Turn off PSM
    if (!writeCommandAndWaitOK("AT+CPSMS=0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 1b");
        return false;
    }

    // Turn off eDRX
    if (!writeCommandAndWaitOK("AT+CEDRXS=0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 1c");
        return false;
    }

    // Set error format
    if (!writeCommandAndWaitOK("AT+CMEE=1\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 2");
        return false;
    }

    // band selection
    if (band == USE_DOCOMO)
    {
        if (!writeCommandAndWaitOK(DOCOMO_CONFIG, MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("init error 3");
            return false;
        }
        if (!writeCommandAndWaitOK(DOCOMO_BAND, MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("init error 3a");
            return false;
        }
    }
    else if (band == USE_KDDI)
    {
        if (!writeCommandAndWaitOK(KDDI_CONFIG, MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("init error 4");
            return false;
        }
        if (!writeCommandAndWaitOK(KDDI_BAND, MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("init error 4a");
            return false;
        }
    }
    else
    {
        if (!writeCommandAndWaitOK(SOFTBANK_CONFIG, MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("init error 5");
            return false;
        }
        if (!writeCommandAndWaitOK(SOFTBANK_BAND, MODEM_LONG_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("init error 5a");
            return false;
        }
    }

    // Enable Network Registration Status
    if (!writeCommandAndWaitOK("AT+CEREG=1\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 6");
        return false;
    }

    return true;
}

bool HL7800Modem::waitLteModem()
{
    for (int i = 0; i < 10; i++)
    {
        if (writeCommandAndWaitOK("AT\r", MODEM_READ_TIMEOUT))
            return true;
        vTaskDelay(500);
    }
    return false;
}

bool HL7800Modem::activate(const char *accessPointName, const char *userName, const char *password)
{
    // Enable Tx/Rx
    if (!writeCommandAndWaitOK("AT+CFUN=1\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 1");
        return false;
    }

    // Set PDP
    sprintf(_cmd, "AT+KCNXCFG=1,\"GPRS\",\"%s\",\"%s\",\"%s\",\"IPV4\"\r", accessPointName, userName, password);
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 2");
        return false;
    }
	
    // Define PDP Context
    sprintf(_cmd, "AT+CGDCONT=1,\"IP\",\"%s\"\r", accessPointName);
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 2");
        return false;
    }

#if 0
    // PDP Context Authentication Configuration
    sprintf(_cmd, "AT+WPPP=2,1,\"%s\",\"%s\"\r", userName, password); // CHAP
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 2");
        return false;
    }
#endif
	
    // PDP connection Up
    if (!writeCommandAndWaitOK("AT+KCNXUP=1\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("activate error 3");
        return false;
    }

    // Wait registration
    if (!readResponse("+KCNX_IND: 1,1,0\r", MODEM_ACTIVATE_TIMEOUT, NULL))
    {
        ERR_DEBUG_PRINTLN("activate error 4");
        return false;
    }

    _Activated = true;

    return true;
}

bool HL7800Modem::deactivate()
{
    bool ret = false;
    if (_Activated)
    {
        if (!writeCommandAndWaitOK("AT+KCNXDOWN=1,1\r", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("deactivate error");
        }
        else
        {
            ret = true;
        }
    }
    _Activated = false;

    return ret;
}

int HL7800Modem::getRssi(void)
{
    int ret = -1;
    if (!writeCommand("AT+CSQ\r"))
        return ret;

    string response;
    if (!readResponse("+CSQ:", MODEM_SHORT_TIMEOUT, &response))
        return false;

    if (response.find("+CSQ:") != string::npos)
    {
        char *head = serchResp(response.c_str(), "+CSQ: ", ",");
        if (head != NULL)
        {
            ret = atoi(head);
            switch (ret)
            {
            case 99:
                ret = 0;
                break;
            case 0:
                ret = -113;
                break;
            case 31:
                ret = -51;
                break;
            default:
                ret = (ret - 1) * 2 - 111;
                break;
            }
        }
    }

    return ret;
}

bool HL7800Modem::getIPaddress(char *add)
{
    bool ret = false;
    if (!writeCommand("AT+KCGPADDR\r"))
        return ret;

    string response;
    if (!readResponse("+KCGPADDR:", MODEM_SHORT_TIMEOUT, &response))
        return false;

    if (response.find("+KCGPADDR: 1") != std::string::npos)
    {
        char *head = serchResp(response.c_str(), "+KCGPADDR: 1,\"", "\"");
        if (head != NULL)
        {
            strcpy(add, head);
            ret = true;
        }
    }

    return ret;
}

bool HL7800Modem::checkBaseStation()
{
    bool ret = false;
    if (!writeCommand("AT+CEREG?\r"))
        return ret;

    string response;
    if (!readResponse("+CEREG:", MODEM_SHORT_TIMEOUT, &response))
        return false;
    if (response.find("+CEREG:") != string::npos)
    {
        if (response[response.size() - 1] == '1')
        {
            ret = true;
        }
    }

    return ret;
}

bool HL7800Modem::checkConnetionTCP(int sessionID)
{
    bool ret = false;
    std::string response;

    // Check connected host and port
    sprintf(_cmd, "AT+KTCPSTAT=%d\r", sessionID);
    if (!writeCommandAndReadResponse(_cmd, "+KTCPSTAT:", MODEM_SHORT_TIMEOUT, &response))
        return false;

    if (response.find("+KTCPSTAT:") != std::string::npos)
    {
        vector<string> v = split(response, ',');
        if (v[0].c_str()[v[0].size() - 1] == '3')
        {
            ret = true;
        }

        _recvSize = atoi(v[3].c_str());
        if (_recvSize > 0)
        {
            _checkSize = false;
        }
        else
        {
            _checkSize = true;
        }
    }

    return ret;
}

bool HL7800Modem::checkConnetionUDP(int sessionID)
{
    return false;
}

bool HL7800Modem::checkConnetion(int sessionID)
{
    if (_type == SocketType::SOCKET_TCP)
    {
        return checkConnetionTCP(sessionID);
    }
    else
    {
        return checkConnetionUDP(sessionID);
    }
}

// for client
int HL7800Modem::socketOpen(const char *host, int port, SocketType type)
{
    if (port < 0 || 65535 < port)
        return 0;

    _type = type;

    if (_type == SocketType::SOCKET_TCP)
    {
        return socketOpenTCP(host, port);
    }
    else
    {
        return socketOpenUDP(host, port);
    }
}

bool HL7800Modem::socketSend(int sessionID, const byte *data, int dataSize)
{
    if (sessionID == 0)
        return false;

    if (_type == SocketType::SOCKET_TCP)
    {
        return socketSendTCP(sessionID, data, dataSize);
    }
    else
    {
        return socketSendUDP(sessionID, data, dataSize);
    }
}

int HL7800Modem::socketReceive(int sessionID, byte *data, int dataSize)
{
    if (sessionID == 0)
        return 0;

    if (_type == SocketType::SOCKET_TCP)
    {
        return socketReceiveTCP(sessionID, data, dataSize);
    }
    else
    {
        return socketReceiveUDP(sessionID, data, dataSize);
    }
}

bool HL7800Modem::socketClose(int sessionID)
{
    if (sessionID == 0)
        return 0;

    if (_type == SocketType::SOCKET_TCP)
    {
        return socketCloseTCP(sessionID);
    }
    else
    {
        return socketCloseUDP(sessionID);
    }
}

bool HL7800Modem::setCACert(const char *rootCA)
{
    int dataSize = strlen(rootCA);
    if (dataSize > 4096)
    {
        ERR_DEBUG_PRINTLN("setCACert size error");
        return false;
    }

    if (!writeCommandAndWaitOK("AT+KCERTDELETE=0,0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCACert error 0");
        return false;
    }

    sprintf(_cmd, "AT+KCERTSTORE=0,%d\r", dataSize);
    if (!writeCommandAndReadResponse(_cmd, "CONNECT", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCACert error 1");
        return false;
    }

    writeBinary((uint8_t *)rootCA, dataSize, MODEM_SHORT_TIMEOUT);

    if (!writeCommandAndWaitOK(ATDATA_EOF, MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCACert error 2");
        return false;
    }

    _SecureMode = true;
    return true;
}

bool HL7800Modem::setCertificate(const char *client_ca)
{
    int dataSize = strlen(client_ca);
    if (dataSize > 4096)
    {
        ERR_DEBUG_PRINTLN("setCertificate size error");
        return false;
    }

    if (!writeCommandAndWaitOK("AT+KCERTDELETE=1,0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCertificate error 0");
        return false;
    }

    sprintf(_cmd, "AT+KCERTSTORE=1,%d\r", dataSize);
    if (!writeCommandAndReadResponse(_cmd, "CONNECT", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCertificate error 1");
        return false;
    }

    writeBinary((uint8_t *)client_ca, dataSize, MODEM_SHORT_TIMEOUT);

    if (!writeCommandAndWaitOK(ATDATA_EOF, MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setCertificate error 2");
        return false;
    }

    _SecureMode = true;
    return true;
}

bool HL7800Modem::setPrivateKey(const char *private_key)
{
    int dataSize = strlen(private_key);
    if (dataSize > 4096)
    {
        ERR_DEBUG_PRINTLN("setPrivateKey size error");
        return false;
    }

    if (!writeCommandAndWaitOK("AT+KPRIVKDELETE=0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setPrivateKey error 0");
        return false;
    }

    sprintf(_cmd, "AT+KPRIVKSTORE=0,%d\r", dataSize);
    if (!writeCommandAndReadResponse(_cmd, "CONNECT", MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setPrivateKey error 1");
        return false;
    }

    writeBinary((uint8_t *)private_key, dataSize, MODEM_SHORT_TIMEOUT);

    if (!writeCommandAndWaitOK(ATDATA_EOF, MODEM_LONG_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("setPrivateKey error 2");
        return false;
    }

    _SecureMode = true;
    return true;
}

#if 0
bool HL7800Modem::changeUartSpeed(UartSpeed speed)
{
    sprintf(_cmd, "AT+IPR=%ld\r", (unsigned long)speed);
    if (!writeCommandAndWaitOK(_cmd, MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("spped change error");
        return false;
    }

    //m_pUart->updateBaudRate((unsigned long)speed);
    m_pUart->end();
    m_pUart->begin((unsigned long)speed);

    delay(2000);

    return waitLteModem();
}
#endif

bool HL7800Modem::sleep(void)
{

#ifdef POWER_SAVE_PSM
    // Turn on PSM
    if (!writeCommandAndWaitOK("AT+CPSMS=1,,,\"00000110\",\"00100010\"\r", MODEM_SHORT_TIMEOUT)) // Sleep 10min x 6, Active 1min X 2 N:2mA
    {
        ERR_DEBUG_PRINTLN("sleep error 3");
        return false;
    }
#else
    // set eDRX mode
    if (!writeCommandAndWaitOK("AT+CEDRXS=1,4,13\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("sleep error1");
        return false;
    }
#endif

    // set Hibernate mode
    if (!writeCommandAndWaitOK("AT+KSLEEP=1,2\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("sleep error2");
        return false;
    }

    digitalWrite(RF_WAKEUP_OUT, LOW);

    return true;
}

bool HL7800Modem::wakeup(void)
{
    digitalWrite(RF_WAKEUP_OUT, HIGH);
    vTaskDelay(20);

    if (!waitLteModem())
    {
        ERR_DEBUG_PRINTLN("wakeup error 1");
        return false;
    }

    // disable Hibernate mode
    if (!writeCommandAndWaitOK("AT+KSLEEP=2\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("wakeup error 2");
        return false;
    }
#ifdef POWER_SAVE_PSM
    // Turn off PSM
    if (!writeCommandAndWaitOK("AT+CPSMS=0\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("init error 1b");
        return false;
    }
#else
    // disable eDRX mode
    if (!writeCommandAndWaitOK("AT+CEDRXS=0,4,5\r", MODEM_SHORT_TIMEOUT))
    {
        ERR_DEBUG_PRINTLN("wakeup error 3");
        return false;
    }
#endif

    return true;
}

bool HL7800Modem::version(int mode, std::string *response)
{
    switch (mode)
    {
    case 0:
        if (!writeCommandAndReadResponse("AT+KGSN=2\r", "+KGSN:", MODEM_SHORT_TIMEOUT, response))
            return false;
        break;
    case 1:
        if (!writeCommandAndReadResponse("ATI3\r", "HL7800", MODEM_SHORT_TIMEOUT, response))
            return false;
        break;
    case 2:
        if (!writeCommandAndReadResponse("ATI8\r", "HL7800", MODEM_SHORT_TIMEOUT, response))
            return false;
        break;
    default:
        return false;
    }

    return true;
}

bool HL7800Modem::getlocaltime(std::string *response)
{
    std::string localtime;
    if (!writeCommandAndReadResponse("AT+CCLK?\r", "+CCLK:", MODEM_SHORT_TIMEOUT, &localtime))
        return false;

    // localtime=> +CCLK: "21/06/04,09:40:02+36" Year/Month/Day,Hour:Min:Sec+GMT
    // resize=> 21/06/04,09:40:02
    *response = localtime.substr(8, 17);
    return true;
}

bool HL7800Modem::gpsMode(bool onoff)
{
    if (onoff)
    {
#if 0 // use A-GPS
        if (!writeCommandAndWaitPattern("AT+CFUN=1\r", "+CEREG: 1", MODEM_ACTIVATE_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 0");
        }
        if (!writeCommandAndWaitOK("AT+GNSSAD=0\r", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 0a");
        }
        if (!writeCommandAndWaitOK("AT+GNSSAD=1,2\r", MODEM_LONG_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 0b");
        }
#endif
        if (!writeCommandAndWaitOK("AT+CFUN=0\r", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 1");
            return false;
        }
        if (!writeCommandAndWaitOK("AT+GNSSCONF=1,0\r", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 2");
            return false;
        }
        if (!writeCommandAndWaitOK("AT+GNSSCONF=10,1\r", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 3");
            return false;
        }
        if (!writeCommandAndWaitOK("AT+GNSSNMEA=0,1000,0,9\r", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 4");
            return false;
        }
        if (!writeCommandAndWaitPattern("AT+GNSSSTART=0\r", "+GNSSEV: 3", MODEM_ACTIVATE_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 5");
            return false;
        }
        if (!writeCommandAndWaitPattern("AT+GNSSNMEA=04\r", "CONNECT", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode on error 6");
            return false;
        }
    }
    else
    {
        if (!writeCommandAndWaitOK("+++", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode off error 1");
            return false;
        }
        if (!writeCommandAndWaitPattern("AT+GNSSSTOP\r", "+GNSSEV: 2,1", MODEM_SHORT_TIMEOUT))
        {
            ERR_DEBUG_PRINTLN("gpsMode off error 2");
            return false;
        }
    }

    return true;
}

int HL7800Modem::band(void)
{
    if (!writeCommand("AT+KBND?\r"))
        return 0;

    string response;
    if (!readResponse("+KBND: 0", MODEM_SHORT_TIMEOUT, &response))
        return 0;

    vector<string> v = split(response, ',');
    int band = atoi(v[1].c_str());
    if (band == 1)
        band = 1;
    else if (band == 2)
        band = 2;
    else if (band == 4)
        band = 3;
    else if (band == 8)
        band = 4;
    else if (band == 10)
        band = 5;
    else if (band == 80)
        band = 8;
    else if (band == 100)
        band = 9;
    else if (band == 200)
        band = 10;
    else if (band == 800)
        band = 12;
    else if (band == 1000)
        band = 13;
    else if (band == 10000)
        band = 17;
    else if (band == 20000)
        band = 18;
    else if (band == 40000)
        band = 19;
    else if (band == 80000)
        band = 20;
    else if (band == 1000000)
        band = 25;
    else if (band == 2000000)
        band = 26;
    else if (band == 4000000)
        band = 27;
    else if (band == 8000000)
        band = 28;
    else
        band = 0;

    return band;
}

bool HL7800Modem::getPhoneNumber(char *num)
{
    bool ret = false;
    std::string resp;
    if (!writeCommandAndReadResponse("AT+CNUM\r", "+CNUM:", MODEM_SHORT_TIMEOUT, &resp))
        return false;

    if (resp.find("+CNUM: ,") != std::string::npos)
    {
        char *head = serchResp(resp.c_str(), "+CNUM: ,\"", "\"");
        if (head != NULL)
        {
            strcpy(num, head);
            ret = true;
        }
    }

    return ret;
}
