/*
  HL7800ModemClient.h - Client class for HL7800(Sierra Wireless)
  Copyright (c) 2020 CircuitDesign,Inc.  All right reserved.
  
  Based programs are as follow
  https://github.com/SeeedJP/Wio_cell_lib_for_Arduino

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#pragma once

#include "HL7800Modem.h"
#include <Client.h>

class HL7800ModemClient : public Client
{

protected:
    HL7800Modem *_ltem;
    int _sessionID;
    byte *_ReceiveBuffer;
    
    byte *_ringBuffer;
    int buffer_size, head, tail, data_length;
    int GetDataLength();
    byte pop();

public:
    HL7800ModemClient(HL7800Modem *ltem);
    virtual ~HL7800ModemClient();

    virtual int connect(IPAddress ip, uint16_t port);
    virtual int connect(const char *host, uint16_t port);
    virtual size_t write(uint8_t data);
    virtual size_t write(const uint8_t *buf, size_t size);
    virtual int available();
    virtual int read();
    virtual int read(uint8_t *buf, size_t size);
    virtual int peek();
    virtual void flush();
    virtual void stop();
    virtual uint8_t connected();
    virtual operator bool();

    bool setCACert(const char *rootCA);
    bool setCertificate(const char *client_ca);
    bool setPrivateKey(const char *private_key);
};
