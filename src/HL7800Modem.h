/*
  HL7800Modem.h - Base class for HL7800(Sierra Wireless)
  Copyright (c) 2020 CircuitDesign,Inc.  All right reserved.
  
  Based programs are as follow
  https://github.com/SeeedJP/Wio_cell_lib_for_Arduino

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
#pragma once

#include <docodemo.h>

#include <IPAddress.h>
#include <time.h>
#include <vector>
#include <string>
#include <map>

class HttpHeader : public std::map<String, String>
{
};

#include "tools/Stopwatch.h"

//#define POWER_SAVE_PSM  //only PSM or eDRX,not both 
//#define DEBUG_MODEM

#ifdef DEBUG_MODEM
#define DEBUG_PRINTLN(msg) SerialDebug.println(msg)
#define DEBUG_PRINT(msg)   SerialDebug.print(msg)
#else
#define DEBUG_PRINTLN(msg)
#define DEBUG_PRINT(msg)
#endif

#define ERR_DEBUG_PRINTLN(msg) SerialDebug.println(msg)
#define ERR_DEBUG_PRINT(msg) SerialDebug.print(msg)

#define MODEM_READ_TIMEOUT 500       // 500mSec
#define MODEM_SHORT_TIMEOUT 3000       // 3sec
#define MODEM_LONG_TIMEOUT 10000       // 10sec
#define MODEM_ACTIVATE_TIMEOUT 60000   // 60sec
#define MODEM_CONNECT_TIMEOUT 60000    // 60sec
#define MODEM_DISCONNECT_TIMEOUT 40000 // 40sec

#define READ_BYTE_TIMEOUT (10)
#define RESPONSE_MAX_LENGTH (128)

#define CHAR_CR (0x0d)
#define CHAR_LF (0x0a)

const char ATRSP_OK[] = "^OK$";
const char ATDATA_EOF[] = "--EOF--Pattern--\r";

const char DOCOMO_CONFIG[] = "AT+KCARRIERCFG=7\r";
const char KDDI_CONFIG[] = "AT+KCARRIERCFG=4\r";
const char SOFTBANK_CONFIG[] = "AT+KCARRIERCFG=8\r";

const char DOCOMO_BAND[] =   "AT+KBNDCFG=0,00000000000008040005\r";//1,3,19,28
const char KDDI_BAND[] =     "AT+KBNDCFG=0,00000000000002020005\r";//1,3,18,26
const char SOFTBANK_BAND[] = "AT+KBNDCFG=0,00000000000008000085\r";//1,3,8,28

typedef int (*UartWrite)(const uint8_t *data, uint32_t len);
typedef int (*UarttRead)(uint8_t *data, uint32_t len, uint32_t timeout);

class HL7800Modem
{
private:
    //HardwareSerial *m_pUart;
    bool _Activated;
    bool _EchoOn;
    char *_cmd;
    bool _SecureMode;
    int _type;
    UartWrite _pWrite;
    UarttRead _pRead;
    bool _checkSize;
    int _recvSize;

    bool waitLteModem();
    bool readResponseUntilOk(unsigned long timeout);
    bool waitForAvailable(Stopwatch *sw, unsigned long timeout);
    bool writeCommand(const char *command);
    bool writeBinary(const uint8_t *pData, int length, unsigned long timeout_ms);
    bool readResponse(const char *pattern, unsigned long timeout, std::string *capture, bool WaitOK = true, bool checkNotIf = false);
    bool writeCommandAndWaitOK(const char *command, unsigned long timeout);
    bool writeCommandAndReadResponse(const char *command, const char *pattern, unsigned long timeout, std::string *capture = NULL);
    bool writeCommandAndWaitPattern(const char *command, const char *pattern, unsigned long timeout);
    bool readBinary(byte *data, int dataSize, unsigned long timeout);

    int socketOpenTCP(const char *host, int port);
    int socketOpenUDP(const char *host, int port);
    bool socketSendTCP(int sessionID, const byte *data, int dataSize);
    bool socketSendUDP(int sessionID, const byte *data, int dataSize);
    int socketReceiveTCP(int sessionID, byte *data, int dataSize);
    int socketReceiveUDP(int sessionID, byte *data, int dataSize);
    bool socketCloseTCP(int sessionID);
    bool socketCloseUDP(int sessionID);

    bool checkConnetionTCP(int sessionID);
    bool checkConnetionUDP(int sessionID);

public:
    enum SocketType
    {
        SOCKET_TCP,
        SOCKET_UDP,
        SOCKET_HTTP,
    };

    enum Channel
    {
        USE_DOCOMO,
        USE_KDDI,
        USE_SOFTBANK
    };

    enum UartSpeed
    {
        BPS1200 = 1200,
        BPS2400 = 2400,
        BPS4800 = 4800,
        BPS9600 = 9600,
        BPS19200 = 19200,
        BPS38400 = 38400,
        BPS57600 = 57600,
        BPS115200 = 115200, //(default value)
        BPS230400 = 230400,
        BPS460800 = 460800,
        BPS921600 = 921600
    };

    enum SOCKET_State
    {
        SOCKET_NOT_DEFINED,
        SOCKET_NOT_USED,
        SOCKET_CONNECTING,
        SOCKET_CONNECTED,
        SOCKET_CLOSING,
        SOCKET_CLOSED,
        SOCKET_ERROR
    };

    HL7800Modem();
    ~HL7800Modem();
    
    //bool init(HardwareSerial &pUart, Channel chan = Channel::USE_DOCOMO);
    bool init(UartWrite pWriteFunc, UarttRead pReadFunc, Channel chan = Channel::USE_DOCOMO);
    bool activate(const char *accessPointName, const char *userName, const char *password);
    bool deactivate();
    int getRssi(void);
    bool getIPaddress(char * add);
    bool checkBaseStation();
    bool checkConnetion(int sessionID);
    bool changeUartSpeed(UartSpeed speed);
    bool version(int mode, std::string *response);
    bool getlocaltime(std::string *response);
    bool sleep();
    bool wakeup(void);
	bool getPhoneNumber(char *num);

    //for client
    int socketOpen(const char *host, int port, SocketType type = SocketType::SOCKET_TCP);
    bool socketSend(int sessionID, const byte *data, int dataSize);
    int socketReceive(int sessionID, byte *data, int dataSize);
    bool socketClose(int sessionID);

    bool setCACert(const char *rootCA);
    bool setCertificate(const char *client_ca);
    bool setPrivateKey(const char *private_key);

    bool gpsMode(bool onoff);
    int band(void);
};
